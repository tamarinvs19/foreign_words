# Foreign words

This project can help to learn any words by heart on the web-site.


* Complete trainings and trials, learn and repeat words. Customize the number of words, the direction, choose dictionaries!
* Continue on your smartphone, the interface is adapted for small displays.
![Training](static/img/Screenshot_2021-02-12 Training.png)
![Trial](static/img/Screenshot_2021-02-12 Trial.png)
                
* You can  join to the different groups or create your. There are private and open groups.
![Groups](static/img/Screenshot_2021-02-12 Groups.png)

* Add private dictionaries inside your groups, create tasks and monitor your progress!
![Dict](static/img/Screenshot_2021-02-12 English SPbSU MCS second-year T1.png)

* Choose various dictionaries or add new!
![Dicts](static/img/Screenshot_2021-02-12 Choose a dictionary.png)

* You can pause at any moment and resume an exercise after. All unfinished tasks are shown on the profile page.
 Also you can explore statistics by all dictionaries in same place.
                
