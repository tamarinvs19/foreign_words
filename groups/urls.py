from django.urls import path

from groups import views

app_name = 'groups'
urlpatterns = [
    path('', views.get_groups, name='get_groups'),
    path('create_group/', views.create_group, name='create_group'),

    path('<int:group_id>', views.group_page, name='group_page'),
    path('<int:group_id>/join_group/', views.join_group, name='join_group'),
    path('<int:group_id>/leave_group/', views.leave_group, name='leave_group'),
    path('<int:group_id>/participants/', views.group_participants, name='participants'),
    path('<int:group_id>/add_student/', views.add_student, name='add_student'),
    path('<int:group_id>/dictionaries/', views.choose_group_dictionary, name='group_dictionaries'),
    path('<int:group_id>/create_dictionary/', views.create_group_dictionary, name='create_group_dictionary'),
    path('<int:group_id>/create_task/', views.create_task, name='create_task'),
    path('<int:group_id>/delete_task/<int:task_id>', views.delete_task, name='delete_task'),

    path('<int:group_id>/rating/', views.get_rating_data, name='get_rating_data'),
]
