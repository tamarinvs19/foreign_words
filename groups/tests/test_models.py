from django.test import TestCase

from groups.models import Group
from users.models import Person


class TestGroups(TestCase):
    def setUp(self) -> None:
        self.users = [
            Person.objects.create(username='Test1', id=1),
            Person.objects.create(username='Test2', id=2),
            Person.objects.create(username='Test3', id=3),
            Person.objects.create(username='Test4', id=4),
        ]
        for user in self.users:
            user.save()

        self.group = Group.objects.create(
            title='test group',
            id=1,
            teacher=self.users[0],
        )
        self.group.students.add(self.users[1])
        self.group.students.add(self.users[2])
        self.group.save()

    def test_contains_true(self):
        self.assertTrue(self.group.contains_user(self.users[0]))
        self.assertTrue(self.group.contains_user(self.users[1]))
        self.assertTrue(self.group.contains_user(self.users[2]))

        self.assertTrue(self.users[0] in self.group)
        self.assertTrue(self.users[1] in self.group)
        self.assertTrue(self.users[2] in self.group)

    def test_contains_false(self):
        self.assertFalse(self.group.contains_user(self.users[3]))
        self.assertFalse(self.users[3] in self.group)

    def test_group_without_teacher(self):
        group = Group.objects.create(
            title='test group',
            id=2,
        )
        group.save()
