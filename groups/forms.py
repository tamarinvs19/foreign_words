import datetime

from django import forms
from django.utils import timezone

from dictionaries.models import Dictionary


class CreateGroupForm(forms.Form):
    title: str = forms.CharField(label='Title', max_length=200)
    description: str = forms.CharField(label='Description', required=False, max_length=1000)
    is_private: bool = forms.BooleanField(label='Private', required=False)


class AddStudentForm(forms.Form):
    username: str = forms.CharField(label='Username', max_length=100)


class CreateTaskForm(forms.Form):
    title: str = forms.CharField(label='Title', max_length=200)
    creation_time: datetime.datetime = forms.DateTimeField(label='Start',
                                                           initial=timezone.now())
    deadline: datetime.datetime = forms.DateTimeField(label='Deadline',
                                                      initial=timezone.now() + datetime.timedelta(days=7))
    dictionary: Dictionary = forms.ModelChoiceField(label='Dictionary', queryset=None)

    def __init__(self, dictionaries, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['dictionary'].queryset = dictionaries
