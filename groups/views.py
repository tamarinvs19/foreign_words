import datetime
from random import randint, seed

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.shortcuts import render
from django.http import HttpResponseRedirect, Http404, JsonResponse
from django.utils import timezone
from django.views.decorators.http import require_http_methods, require_GET, require_POST
from icecream import ic

from groups.form_handler import FormHandler
from groups.forms import CreateGroupForm, AddStudentForm, CreateTaskForm
from groups.models import Group
from dictionaries.form_handler import FormHandler as DictionaryFormHandler
from groups.form_handler import FormHandler as TasksFormHandler
from dictionaries.forms import CreateDictionaryForm
from dictionaries.models import Dictionary
from tasks.models import Task, RatingData
from users.models import Person


@login_required
@require_GET
def get_rating_data(request, group_id) -> JsonResponse:
    group: Group = Group.objects.get(id=group_id)
    time: int = 10
    rating_data: RatingData = RatingData(group, time)
    person_progresses: dict[Person, list[(int, int, int)]] = {
        person: rating_data.get_progress(person)
        for person in group.students.all()
    }
    max_progress: list = [max(progress[i][0] for progress in person_progresses.values()) for i in range(time)]

    def calc_progress(progress: (int, int, int), day: int) -> float:
        return 0 if progress[1] == 0 or max_progress[day] == 0 else\
            (progress[0] / progress[1]) * (progress[0] / max_progress[day]) * 5

    def generate_color(key: int) -> str:
        seed(77*key)
        return 'rgba({}, {}, {}, 0.2)'.format(*[randint(0, 255) for _ in range(3)])

    return JsonResponse({
        'dates': [(timezone.now() - datetime.timedelta(days=days)).date() for days in range(time - 1, -1, -1)],
        'dataset': [{
            'label': person.username if not person.first_name else person.first_name,
            'type': 'line',
            'borderColor': generate_color(key),
            'backgroundColor': generate_color(key),
            'data': [calc_progress(person_progresses[person][day], day) for day in range(time-1, -1, -1)],
            }
            for key, person in enumerate(person_progresses.keys())
        ]
    })


@login_required
@require_GET
def get_groups(request):
    user: User = request.user
    groups: list[Group] = list(filter(lambda g: user in g, Group.objects.all()))
    public_groups: list[Group] = list(filter(lambda g: user not in g, Group.objects.filter(is_private=False)))
    return render(request, 'groups/group_list.html', {'groups': groups, 'public_groups': public_groups})


@login_required
@require_GET
def group_page(request, group_id):
    user: User = request.user
    person: Person = Person.objects.get(id=user.id)
    group: Group = Group.objects.get(id=group_id)
    if user in group:
        tasks: list[Task] = Task.objects.filter(group=group,
                                                deadline__gt=timezone.now(),
                                                creation_time__lt=timezone.now())
        tasks_with_progress: list[(Task, int)] = [(task, task.calculate_progress(person)) for task in tasks]
        return render(request, 'groups/group_page.html', {
            'group': group,
            'new_tasks': tasks_with_progress,
        })
    else:
        return Http404


@login_required
@require_http_methods(['DELETE'])
def delete_task(request, group_id, task_id):
    user: User = request.user
    group: Group = Group.objects.get(id=group_id)
    if user in group:
        if len(list(Task.objects.filter(id=task_id, group=group))) == 1:
            task: Task = Task.objects.get(id=task_id)
            task_id: int = task.id
            task.delete()
            messages.add_message(request, messages.INFO, 'Task was deleted')
            return JsonResponse({'status': 'ok', 'task_id': task_id})
        else:
            messages.add_message(request, messages.INFO, 'This task does not exists')
            return JsonResponse({'status': 'error'})
    return Http404


@login_required
@require_GET
def choose_group_dictionary(request, group_id):
    user: User = request.user
    group: Group = Group.objects.get(id=group_id)
    if user in group:
        return render(request, 'exercises/choose_dictionary.html',
                      {'dictionaries': Dictionary.objects.filter(group=group),
                       'group': group
                       })
    else:
        return Http404


@login_required
@require_http_methods(['GET', 'POST'])
def create_group_dictionary(request, group_id):
    group: Group = Group.objects.get(id=group_id)
    user: User = request.user
    if user in group:
        if request.method == 'POST':
            form: CreateDictionaryForm = CreateDictionaryForm(request.POST, request.FILES, group)
            if form.is_valid():
                DictionaryFormHandler.save_dictionary(form, group)
                return HttpResponseRedirect('/groups/{group_id}/dictionaries/'.format(group_id=group_id))
        else:
            form: CreateDictionaryForm = CreateDictionaryForm()
            return render(request, 'dictionaries/create_dictionary.html', {'form': form})


@login_required()
@require_http_methods(['GET', 'POST'])
def create_group(request):
    user: User = request.user
    person: Person = Person.objects.get(email=user.email)
    if request.method == 'POST':
        form: CreateGroupForm = CreateGroupForm(request.POST)
        if form.is_valid():
            FormHandler.create_group(person, form)
            return HttpResponseRedirect('/groups/')
    else:
        form: CreateGroupForm = CreateGroupForm()
        return render(request, 'groups/create_group.html', {'form': form})


@login_required
@require_GET
def group_participants(request, group_id):
    group: Group = Group.objects.get(id=group_id)
    return render(request, 'groups/group_participants.html', {'group': group})


@login_required()
@require_http_methods(['GET', 'POST'])
def add_student(request, group_id):
    group: Group = Group.objects.get(id=group_id)
    if request.method == 'POST':
        form: AddStudentForm = AddStudentForm(request.POST)
        if form.is_valid():
            username: str = form.cleaned_data['username']
            if len(Person.objects.filter(username=username).all()) == 1:
                group.students.add(Person.objects.get(username=username))
                return HttpResponseRedirect('/groups/{}'.format(group_id))
            else:
                return render(request, 'groups/add_student.html',
                              {'form': form, 'group': group,
                               'messages': ['No students has a username {}'.format(username)]})
    else:
        form: AddStudentForm = AddStudentForm()
        return render(request, 'groups/add_student.html', {'form': form, 'group': group})


@login_required
@require_GET
def join_group(request, group_id):
    user: User = request.user
    person: Person = Person.objects.get(email=user.email)
    group: Group = Group.objects.get(id=group_id)
    group.students.add(person)
    group.save()
    messages.add_message(request, messages.INFO, 'You have joined "{}"'.format(group.title))
    return HttpResponseRedirect('/groups/{}'.format(group_id))


@login_required
@require_GET
def leave_group(request, group_id):
    user: User = request.user
    person: Person = Person.objects.get(email=user.email)
    group: Group = Group.objects.get(id=group_id)
    group.students.remove(person)
    group.save()
    messages.add_message(request, messages.INFO, 'You left "{}"'.format(group.title))
    return HttpResponseRedirect('/groups/')


@login_required
@require_http_methods(['GET', 'POST'])
def create_task(request, group_id):
    group: Group = Group.objects.get(id=group_id)
    user: User = request.user
    person: Person = Person.objects.get(id=user.id)
    dictionaries: list[Dictionary] = Dictionary.objects.filter(group=group)
    if user in group:
        if request.method == 'POST':
            form: CreateTaskForm = CreateTaskForm(dictionaries, request.POST)
            if form.is_valid():
                TasksFormHandler.create_task(person, group, form)
                return HttpResponseRedirect('/groups/{group_id}'.format(group_id=group_id))
        else:
            form: CreateTaskForm = CreateTaskForm(dictionaries)
            return render(request, 'groups/create_task.html', {'form': form})
