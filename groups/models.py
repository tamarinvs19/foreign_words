from __future__ import annotations

from django.contrib.auth.models import User
from django.db import models

from users.models import Person


class Group(models.Model):
    title: str = models.CharField(max_length=200)
    students = models.ManyToManyField('users.Person', related_name='+')
    teacher: Person = models.ForeignKey('users.Person', on_delete=models.CASCADE, blank=True, null=True)

    description: str = models.CharField(max_length=1000, default='')

    is_private: bool = models.BooleanField(default=False)
    creator: Person = models.ForeignKey('users.Person', on_delete=models.CASCADE,
                                        blank=True, null=True, related_name='creator')

    def contains_user(self, user: User):
        return user in self.students.all() or user == self.teacher

    def __contains__(self, item: User) -> bool:
        return self.contains_user(item)

    def __repr__(self) -> str:
        return 'Group(title={})'.format(self.title)

    def __str__(self) -> str:
        return 'Group: {}'.format(self.title)
