import datetime

from dictionaries.models import Dictionary
from groups.forms import CreateGroupForm, CreateTaskForm
from groups.models import Group
from tasks.models import Task
from users.models import Person


class FormHandler(object):
    @staticmethod
    def create_group(creator: Person, form: CreateGroupForm) -> None:
        cd: dict = form.cleaned_data
        is_private: bool = cd['is_private']
        title: str = cd['title']
        description: str = cd['description']
        group: Group = Group.objects.create(creator=creator, is_private=is_private,
                                            title=title, description=description)
        group.students.add(creator)
        group.save()

    @staticmethod
    def create_task(creator: Person, group: Group, form: CreateTaskForm) -> Task:
        cd: dict = form.cleaned_data
        title: str = cd['title']
        creation_time: datetime.datetime = cd['creation_time']
        deadline: datetime.datetime = cd['deadline']
        dictionary: Dictionary = cd['dictionary']
        task: Task = Task.objects.create(title=title,
                                         creation_time=creation_time,
                                         deadline=deadline,
                                         dictionary=dictionary,
                                         creator=creator,
                                         group=group)
        task.save()
