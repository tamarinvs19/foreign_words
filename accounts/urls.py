from django.urls import path

from . import views

app_name = 'accounts'
urlpatterns = [
    path('', views.get_profile, name='get_profile'),
    path('settings/', views.settings, name='settings'),
    path('statistics/', views.statistics, name='statistics'),
    path('statistic_dict/<int:dict_id>/', views.statistic_dict, name='statistic_dict'),
]
