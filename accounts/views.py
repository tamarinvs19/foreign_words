from operator import itemgetter

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import JsonResponse
from django.shortcuts import render
from django.views.decorators.http import require_GET, require_http_methods

from dictionaries.models import Dictionary, Word
from tasks.models import TrainingExercise, TrialExercise
from tasks.models import get_statistic_list, Statistic


@login_required
@require_GET
def get_profile(request):
    user: User = request.user
    trials: list[TrialExercise] = [task for task in TrialExercise.objects.filter(creator=user).all()
                                   if not task.is_finished]
    trials_id: list[int] = [trial.id for trial in trials]
    trainings: list[TrainingExercise] = [task for task in TrainingExercise.objects.filter(creator=user).all()
                                         if not task.is_finished and task.id not in trials_id]
    return render(request, 'accounts/profile.html', {'user': user,
                                                     'unfinished_trainings': trainings,
                                                     'unfinished_trials': trials,
                                                     })


@login_required
@require_http_methods(['GET', 'POST'])
def settings(request):
    user: User = request.user
    if request.method == 'GET':
        return render(request, 'accounts/settings.html', {'user': user})
    elif request.method == 'POST':
        new_username: str = request.POST['username']
        new_firstname: str = request.POST['firstname']
        new_lastname: str = request.POST['lastname']
        user.username = new_username
        user.first_name = new_firstname
        user.last_name = new_lastname
        user.save()
        messages.add_message(request, messages.INFO, 'All changes was saved')
        my_messages = list(map(lambda msg: msg.message, messages.get_messages(request)))
        print(my_messages)
        return JsonResponse({'my_messages': my_messages})


@login_required
@require_GET
def statistics(request):
    user: User = request.user
    used_dicts: set[Dictionary] = {exercise.dictionary
                                   for exercise in TrainingExercise.objects.filter(creator=user)
                                   if exercise.dictionary}
    return render(request, 'accounts/statistics_dict.html', {
        'user': user,
        'dictionaries': list(used_dicts)
    })


@login_required
@require_GET
def statistic_dict(request, dict_id):
    user: User = request.user
    dictionary: Dictionary = Dictionary.objects.get(id=dict_id)
    trials: list[TrialExercise] = list(TrialExercise.objects.filter(creator=user, dictionary=dictionary))
    trials_id: list[int] = [trial.id for trial in trials]
    trainings: list[TrainingExercise] = [exercise for exercise in
                                         TrainingExercise.objects.filter(creator=user, dictionary=dictionary)
                                         if exercise.id not in trials_id]
    training_words: dict[Word, Statistic] = {Word.objects.get(id=word_id): statistic
                                             for word_id, statistic in get_statistic_list(trainings).items()}
    trial_words: dict[Word, Statistic] = {Word.objects.get(id=word_id): statistic
                                          for word_id, statistic in get_statistic_list(trials).items()}
    return render(request, 'accounts/statistics.html', {
        'user': user,
        'trial_words': sorted(trial_words.items(), key=itemgetter(0)),
        'training_words': sorted(training_words.items(), key=itemgetter(0)),
        'language1': dictionary.languages[0],
        'language2': dictionary.languages[1]
    })
