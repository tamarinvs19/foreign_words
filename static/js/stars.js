function show_stars (star_number) {
    const stars_div = document.getElementById("stars");
    const star_size = stars_div.clientWidth / 6;
    const app = new PIXI.Application({
        transparent: true,
        width: stars_div.clientWidth,
        height: stars_div.clientWidth / 4
    });
    stars_div.appendChild(app.view);

    const texture = PIXI.Texture.from('/static/img/drawing.svg');
    const secondTexture = PIXI.Texture.from('/static/img/drawing2.svg');

    let stars = []
    for (let i = 0; i < star_number; i++) {
        stars.push(new PIXI.Sprite(texture));
    }

    for (let i = 0; i < star_number; i++) {
        const star = stars[i];
        star.width = star_size;
        star.height = star_size;
        star.anchor.set(0.5);

        // move the sprite to the center of the screen
        star.goal_x = app.screen.width / 2 + (i - (star_number - 1) / 2) * star.width * (star_number + 1) / star_number;
        star.goal_y = app.screen.height / 2;

        star.x = -100;
        star.y = -100;
        star.speed = 0.1;

        app.stage.addChild(star);

    }
    app.ticker.add(() => {
        for (let i = 0; i < star_number; i++) {
            const star = stars[i];
            if (Math.abs(star.x - star.goal_x) > 0.1) {
                star.rotation += 0.1;
                star.x += (star.goal_x - star.x) * star.speed
                star.y += (star.goal_y - star.y) * star.speed
            } else if (Math.abs(star.x - star.goal_x) > 0.05) {
                star.rotation += 0.025;
                star.x += (star.goal_x - star.x) * star.speed
                star.y += (star.goal_y - star.y) * star.speed
                star.texture = secondTexture;
            } else {
                star.rotation += 0;
            }
        }
    });

}