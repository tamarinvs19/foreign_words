from django.shortcuts import render


def main_page(request):
    return render(request, 'main.html')


def help_page(request):
    return render(request, 'help.html')
