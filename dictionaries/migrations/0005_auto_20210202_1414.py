# Generated by Django 3.1.4 on 2021-02-02 14:14

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('groups', '0004_auto_20210202_1407'),
        ('dictionaries', '0004_auto_20210202_1407'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dictionary',
            name='group',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='groups.group'),
        ),
    ]
