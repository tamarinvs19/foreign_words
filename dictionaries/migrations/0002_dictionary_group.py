# Generated by Django 3.1.4 on 2021-02-02 11:05

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('groups', '0001_initial'),
        ('dictionaries', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='dictionary',
            name='group',
            field=models.OneToOneField(blank=True, on_delete=django.db.models.deletion.CASCADE, to='groups.group'),
        ),
    ]
