import csv
from typing import Optional

from django.utils import timezone
from icecream import ic

from dictionaries.forms import CreateDictionaryForm, UpdateDictionaryForm
from dictionaries.models import Dictionary, Word
from groups.models import Group


class FormHandler(object):
    @staticmethod
    def word(line: dict, language1: str, language2: str) -> Word:
        return Word.objects.create(
            language1=[language1, line[language1]],
            language2=[language2, line[language2]],
            part_of_speech=line['part_of_speech']
        )

    @staticmethod
    def save_dictionary(form: CreateDictionaryForm, group: Optional[Group] = None) -> None:
        directory = 'files'

        cd: dict = form.cleaned_data
        file = cd['table']
        title: str = cd['title']
        language1: str = cd['language1']
        language2: str = cd['language2']
        description: str = cd['description']

        dictionary_path: str = '{}/{}.csv'.format(directory, title)
        with open(dictionary_path, 'wb+') as destination:
            for chunk in file.chunks():
                destination.write(chunk)

        with open(dictionary_path, 'r') as dict_table:

            csv_reader: csv.DictReader = csv.DictReader(dict_table,
                                                        fieldnames=[language1, language2, 'part_of_speech'])
            words_id: list[int] = [FormHandler.word(row, language1, language2).id
                                   for row in csv_reader][1:]

            if group:
                Dictionary.objects.create(
                    title=title,
                    words_id=words_id,
                    languages=(language1, language2),
                    group=group,
                    description=description,
                )
            else:
                Dictionary.objects.create(
                    title=title,
                    words_id=words_id,
                    languages=(language1, language2),
                    description=description,
                )

    @staticmethod
    def update_dictionary(form: UpdateDictionaryForm, dictionary_id: int) -> None:
        directory = 'files'

        cd: dict = form.cleaned_data
        file = cd['table']
        priority: int = int(cd['priority'])
        dictionary: Dictionary = Dictionary.objects.get(id=dictionary_id)

        dictionary_path: str = '{}/{}_update_{}.csv'.format(directory, dictionary.title, timezone.now())
        with open(dictionary_path, 'wb+') as destination:
            for chunk in file.chunks():
                destination.write(chunk)

        language1: str = dictionary.languages[0]
        language2: str = dictionary.languages[1]
        with open(dictionary_path, 'r') as update_dict_table:
            csv_reader: csv.DictReader = csv.DictReader(update_dict_table,
                                                        fieldnames=[
                                                            language1,
                                                            language2,
                                                            'part_of_speech'
                                                        ])
            words_id: list[int] = [FormHandler.word(row, language1, language2).id
                                   for row in csv_reader]
            ic(words_id)

        for word_id in words_id:
            ic(word_id)
            dictionary.update(word_id, priority)
