from django.contrib import admin
from .models import Word, Dictionary

admin.site.register(Word)
admin.site.register(Dictionary)
