from django import forms


class CreateDictionaryForm(forms.Form):
    title: str = forms.CharField(label='Title', max_length=100)
    language1: str = forms.CharField(label='Language 1', max_length=100)
    language2: str = forms.CharField(label='Language 2', max_length=100)
    description: str = forms.CharField(label='Description', max_length=1000, required=False)
    table = forms.FileField()


class UpdateDictionaryForm(forms.Form):
    table = forms.FileField()
    priority: str = forms.ChoiceField(label='Priority', initial=-1,
                                      choices=[
                                          [-1, 'save previous'],
                                          [0, 'save duplicates'],
                                          [1, 'rewrite']
                                      ])
