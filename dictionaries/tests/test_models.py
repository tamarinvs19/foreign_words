import unittest.mock

from django.test import TestCase

from dictionaries.models import WordTemplate, Word, Dictionary
from groups.models import Group
from users.models import Person


class TestWord(TestCase):
    def setUp(self) -> None:
        self.english_word_list: list[WordTemplate] = [
            WordTemplate('english', 'badger'),
            WordTemplate('english', 'run'),
            WordTemplate('english', 'linux'),
        ]
        self.russian_word_list: list[WordTemplate] = [
            WordTemplate('russian', 'барсук'),
            WordTemplate('russian', 'бежать'),
            WordTemplate('russian', 'линукс'),
        ]

    def test_init(self) -> None:
        word: Word = Word(
            language1=self.english_word_list[0],
            language2=self.russian_word_list[0]
        )
        word.save()

        self.assertEqual(word.language1, self.english_word_list[0])
        self.assertEqual(word.language2, self.russian_word_list[0])

    def test_repr(self) -> None:
        word: Word = Word(
            language1=self.english_word_list[0],
            language2=self.russian_word_list[0]
        )
        word.save()
        string_repr: str = 'Word(language1={}, language2={})'.format(
            self.english_word_list[0], self.russian_word_list[0]
        )

        self.assertEqual(word.__repr__(), string_repr)

    def test_get_language(self) -> None:
        word: Word = Word(
            language1=self.english_word_list[0],
            language2=self.russian_word_list[0]
        )
        word.save()

        self.assertEqual(word.get_word_language1, self.english_word_list[0])
        self.assertEqual(word.get_word_language2, self.russian_word_list[0])

    def test_get_languages(self) -> None:
        word: Word = Word(
            language1=self.english_word_list[0],
            language2=self.russian_word_list[0]
        )
        word.save()

        self.assertEqual(word.get_languages, ['english', 'russian'])

    def test_get_word_and_translation(self):
        word: Word = Word(
            language1=self.english_word_list[0],
            language2=self.russian_word_list[0]
        )
        word.save()

        self.assertEqual(
            (self.english_word_list[0].word, self.russian_word_list[0].word),
            word.get_word_and_translation('english')
        )
        self.assertEqual(
            (self.russian_word_list[0].word, self.english_word_list[0].word),
            word.get_word_and_translation('russian')
        )
        with self.assertRaisesMessage(ValueError, 'Nonexistent language') as _:
            word.get_word_and_translation('polish')


class TestDictionary(TestCase):
    def setUp(self) -> None:
        self.english_word_list: list[WordTemplate] = [
            WordTemplate('english', 'badger'),
            WordTemplate('english', 'run'),
            WordTemplate('english', 'linux'),
        ]
        self.russian_word_list: list[WordTemplate] = [
            WordTemplate('russian', 'барсук'),
            WordTemplate('russian', 'бежать'),
            WordTemplate('russian', 'линукс'),
        ]
        self.words: list[Word] = [Word(language1=lang1, language2=lang2)
                                  for lang1, lang2 in
                                  zip(self.english_word_list, self.russian_word_list)
                                  ]
        self.words_id: list[int] = [word.id for word in self.words]
        self.group: Group = Group.objects.create(title='test group')
        self.dictionary: Dictionary = Dictionary(
            title='test dictionary',
            words_id=self.words_id,
            languages=('english', 'russian'),
            description='test',
        )
        self.dictionary.save()

    @unittest.mock.patch('random.randint', side_effect=[0, 1])
    def test_get_random_words_less_max(self, _) -> None:
        generated_words_id: list[int] = self.dictionary.get_random_words(2)
        self.assertEqual(2, len(generated_words_id))
        self.assertEqual(self.words_id[0], generated_words_id[0])
        self.assertEqual(self.words_id[1], generated_words_id[1])

    @unittest.mock.patch('random.randint', side_effect=[0, 1, 2, 0, 0, 2])
    def test_get_random_words_more_max(self, _) -> None:
        generated_words_id: list[int] = self.dictionary.get_random_words(6)
        self.assertEqual(6, len(generated_words_id))
        self.assertEqual(self.words_id[0], generated_words_id[0])
        self.assertEqual(self.words_id[1], generated_words_id[1])
        self.assertEqual(self.words_id[2], generated_words_id[2])
        self.assertEqual(self.words_id[0], generated_words_id[3])
        self.assertEqual(self.words_id[0], generated_words_id[4])
        self.assertEqual(self.words_id[2], generated_words_id[5])

    def test_group(self) -> None:
        user = Person.objects.create(username='Test', id=1)
        user.save()
        group = Group.objects.create(title='test group', teacher=user)
        dictionary: Dictionary = Dictionary(
            title='test dictionary',
            words_id=self.words_id,
            languages=('english', 'russian'),
            group=group,
        )
        dictionary.save()

        self.assertEqual('test group', dictionary.group.title)

    def test_size_1(self) -> None:
        self.assertEqual(3, self.dictionary.size)

    def test_size_2(self) -> None:
        dictionary: Dictionary = Dictionary.objects.create(
            title='test',
            words_id=[1, 2]
        )
        self.assertEqual(2, dictionary.size)

    @unittest.mock.patch('random.randint', side_effect=[0, 1, 2, 0, 0, 2])
    def test_get_random_word(self, _) -> None:
        self.assertEqual(self.words_id[0], self.dictionary.get_random_word())
        self.assertEqual(self.words_id[1], self.dictionary.get_random_word())
        self.assertEqual(self.words_id[2], self.dictionary.get_random_word())
        self.assertEqual(self.words_id[0], self.dictionary.get_random_word())
        self.assertEqual(self.words_id[0], self.dictionary.get_random_word())
        self.assertEqual(self.words_id[2], self.dictionary.get_random_word())
