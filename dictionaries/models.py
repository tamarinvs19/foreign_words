from __future__ import annotations

import random
from collections import namedtuple

from django.db import models
from icecream import ic

from groups.models import Group

WordTemplate = namedtuple('WordTemplate', ['language', 'word'])


class Word(models.Model):
    language1: WordTemplate = models.JSONField(default=WordTemplate)
    language2: WordTemplate = models.JSONField(default=WordTemplate)
    part_of_speech: str = models.CharField(max_length=40, null=True)

    def __repr__(self) -> str:
        return 'Word(language1={}, language2={})'.format(self.get_word_language1, self.get_word_language2)

    def __str__(self) -> str:
        return 'Word: {} - {}'.format(self.get_word_language1.word, self.get_word_language2.word)

    def __lt__(self, other: Word) -> bool:
        return \
            self.language1[0] == other.language1[0] and \
            self.language2[0] == other.language2[0] and \
            (self.language1[1] < other.language1[1] or
             (self.language1[1] == other.language1[1] and
              self.language2[1] < other.language2.word))

    def __le__(self, other: Word) -> bool:
        return self < other or self == other

    def __gt__(self, other: Word) -> bool:
        return not (self == other or self < other)

    def __ge__(self, other: Word) -> bool:
        return not self < other

    def __eq__(self, other: Word) -> bool:
        return \
            self.language1[0] == other.language1[0] and \
            self.language2[0] == other.language2[0] and \
            self.language1[1] == other.language1[1] and \
            self.language2[1] == other.language2.word

    def __hash__(self) -> int:
        return hash(self.__repr__())

    @property
    def get_word_language1(self) -> WordTemplate:
        return WordTemplate(*self.language1)

    @property
    def get_word_language2(self) -> WordTemplate:
        return WordTemplate(*self.language2)

    @property
    def get_languages(self) -> list[str]:
        return [self.get_word_language1.language, self.get_word_language2.language]

    def get_word_and_translation(self, language: str) -> tuple[str, str]:
        if language == self.get_word_language1.language:
            return self.get_word_language1.word, self.get_word_language2.word
        elif language == self.get_word_language2.language:
            return self.get_word_language2.word, self.get_word_language1.word
        else:
            raise ValueError('Nonexistent language')


class Dictionary(models.Model):
    title: str = models.CharField(default='', max_length=100)
    words_id: list[int] = models.JSONField(default=list)
    languages: tuple[str, str] = models.JSONField(default=tuple)
    group: Group = models.ForeignKey('groups.Group', on_delete=models.CASCADE, blank=True, null=True)
    description: str = models.CharField(default='', max_length=1000)

    def __repr__(self) -> str:
        if self.group is Group:
            return 'Dictionary(title={}, group={})'.format(self.title, self.group)
        return 'Dictionary: {}'.format(self.title)

    def __str__(self) -> str:
        if self.group is Group:
            return 'Dictionary: {}, group: {}'.format(self.title, self.group.title)
        return 'Dictionary: {}'.format(self.title)

    @property
    def size(self) -> int:
        return len(self.words_id)

    def get_random_words(self, count_words: int) -> list[int]:
        return [
            self.get_random_word()
            for _ in range(count_words)
        ]

    def get_random_word(self) -> int:
        min_word_number: int = 0
        max_word_number: int = len(self.words_id) - 1
        return self.words_id[random.randint(min_word_number, max_word_number)]

    def update(self, word_id: int, priority: int):
        new_word: Word = Word.objects.get(id=word_id)
        if priority == 0:
            self.words_id.append(word_id)
        elif priority == 1:
            for saved_id in self.words_id:
                saved_word: Word = Word.objects.get(id=saved_id)
                if new_word.get_word_language1.word == saved_word.get_word_language1.word or \
                        new_word.get_word_language2.word == saved_word.get_word_language2.word:
                    self.words_id.remove(saved_id)
                    break
            self.words_id.append(word_id)
        elif priority == -1:
            exists: bool = False
            for saved_id in self.words_id:
                saved_word: Word = Word.objects.get(id=saved_id)
                if new_word.get_word_language1.word == saved_word.get_word_language1.word or \
                        new_word.get_word_language2.word == saved_word.get_word_language2.word:
                    exists: bool = True
                    break
            if not exists:
                self.words_id.append(word_id)
        self.save()
