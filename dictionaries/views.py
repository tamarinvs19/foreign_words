from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from icecream import ic

from dictionaries.forms import CreateDictionaryForm, UpdateDictionaryForm
from .form_handler import FormHandler
from .models import Dictionary


@login_required
@require_http_methods(['GET', 'POST'])
def create_dictionary(request):
    if request.method == 'POST':
        form: CreateDictionaryForm = CreateDictionaryForm(request.POST, request.FILES)
        if form.is_valid():
            FormHandler.save_dictionary(form)
            return HttpResponseRedirect('/tasks/choose_dictionary/')
    else:
        form: CreateDictionaryForm = CreateDictionaryForm()
    return render(request, 'dictionaries/create_dictionary.html', {'form': form})


@login_required
@require_http_methods(['GET', 'POST'])
def update_dictionary(request, dictionary_id):
    dictionary: Dictionary = Dictionary.objects.get(id=dictionary_id)
    if request.method == 'POST':
        form: UpdateDictionaryForm = UpdateDictionaryForm(request.POST, request.FILES)
        if form.is_valid():
            FormHandler.update_dictionary(form, dictionary_id)
            return HttpResponseRedirect('/tasks/choose_dictionary/')
    else:
        form: UpdateDictionaryForm = UpdateDictionaryForm()
    return render(request, 'dictionaries/update_dictionary.html',
                  {'form': form, 'dictionary': dictionary})
