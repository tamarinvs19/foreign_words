from django.urls import path

from . import views

app_name = 'dictionaries'
urlpatterns = [
    path('create_dictionary/', views.create_dictionary, name='create_dictionary'),
    path('update_dictionary/<int:dictionary_id>/', views.update_dictionary, name='update_dictionary'),
]
