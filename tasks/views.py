import json

from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, JsonResponse, Http404
from django.utils import timezone
from django.views.decorators.http import require_http_methods, require_GET

from .form_handler import FormHandler
from .forms import CreateExerciseForm
from .models import TrainingExercise, TrialExercise
from dictionaries.models import Dictionary, Word


@login_required
@require_http_methods(['GET', 'POST'])
def training(request, pk):
    template_name = 'trainings/training_page.html'
    training_exercise: TrainingExercise = TrainingExercise.objects.get(id=pk)
    user = request.user
    if user != training_exercise.creator:
        return Http404
    if not training_exercise.is_finished:
        if request.method == 'GET':
            next_word: Word = training_exercise.next_question()
            word, translation = next_word.get_word_and_translation(training_exercise.language)
            return render(request, template_name, {'word': word, 'translation': translation,
                                                   'language1': training_exercise.language,
                                                   'language2': training_exercise.second_language
                                                   })
        elif request.method == 'POST':
            knowledge: bool = json.loads(request.POST['knowledge'])
            training_exercise.save_result(knowledge)
            return render(request, template_name)
    else:
        return redirect('/tasks/training/{}/result'.format(pk))


@login_required
@require_GET
def training_next(request, pk):
    training_exercise: TrainingExercise = TrainingExercise.objects.get(id=pk)
    user: User = request.user
    if user == training_exercise.creator and not training_exercise.is_finished:
        next_word: Word = training_exercise.next_question()
        word, translation = next_word.get_word_and_translation(training_exercise.language)
        return JsonResponse({'word': word, 'translation': translation})
    else:
        training_exercise.duration = \
            timezone.now() - training_exercise.creation_time
        training_exercise.save()
        return JsonResponse({'stop': True, 'address': '/tasks/training/{}/result'.format(pk)})


@login_required
@require_GET
def choose_dictionary(request):
    return render(request, 'exercises/choose_dictionary.html',
                  {'dictionaries': Dictionary.objects.filter(group=None)})


@login_required
@require_http_methods(['GET', 'POST'])
def create_exercise(request, dict_id):
    dictionary: Dictionary = Dictionary.objects.get(id=dict_id)
    user: User = request.user
    if request.method == 'POST':
        form: CreateExerciseForm = CreateExerciseForm(dictionary, request.POST)
        if form.is_valid():
            mode = form.cleaned_data['mode']
            if mode == 'training':
                training_exercise: TrainingExercise = FormHandler.create_training(user, form, dictionary)
                return HttpResponseRedirect('/tasks/training/{}'.format(training_exercise.id))
            elif mode == 'trial':
                trial_exercise: TrialExercise = FormHandler.create_trial(user, form, dictionary)
                return HttpResponseRedirect('/tasks/trial/{}'.format(trial_exercise.id))
    else:
        form: CreateExerciseForm = CreateExerciseForm(dictionary)
    return render(request, 'exercises/create_exercise.html', {'form': form})


@login_required
@require_GET
def show_results(request, pk):
    training_exercise: TrainingExercise = TrainingExercise.objects.get(id=pk)
    user = request.user
    if user == training_exercise.creator:
        duration = {'minutes': int(training_exercise.duration.total_seconds() // 60),
                    'seconds': training_exercise.duration.seconds % 60}
        return render(request, 'trainings/result_training.html',
                      {
                          'progress': training_exercise.progress,
                          'duration': duration,
                          'group': training_exercise.dictionary.group,
                          'dictionary_id': training_exercise.dictionary_id,
                          'star_count': int(5 * training_exercise.progress[0] / training_exercise.progress[1])
                      })
    else:
        return Http404


@login_required
@require_http_methods(['GET', 'POST'])
def trial(request, pk):
    template_name = 'trials/trial_page.html'
    trial_exercise: TrialExercise = TrialExercise.objects.get(id=pk)
    user = request.user
    if user != trial_exercise.creator:
        return Http404
    if not trial_exercise.is_finished:
        if request.method == 'GET':
            next_word: str
            next_answers: list[str]
            next_word, next_answers = trial_exercise.next_question_and_answers()
            return render(request, template_name, {'question': next_word, 'answers': next_answers,
                                                   'language1': trial_exercise.language,
                                                   'language2': trial_exercise.second_language
                                                   })
        elif request.method == 'POST':
            answer: str = request.POST['answer']
            language: str = trial_exercise.language
            right_answer: str = trial_exercise.get_right_answer().get_word_and_translation(language)[1]
            result: bool = answer == right_answer
            trial_exercise.save_result(result)
            return JsonResponse({'right_answer': right_answer, 'result': result})
    else:
        return redirect('/tasks/trial/{}/result'.format(pk))


def generate_html_for_answers(answers: list[str]) -> str:
    result_list: list[str] = \
        ['<div class="radio-container">'] + \
        [
            ''' <div class="input-div form-item radio-btn nth-6">
                    <input type="radio" name="answer" id="{answer}">
                    <label for="{answer}">{answer}</label>
                </div>
            '''.format(answer=answer)
            for answer in answers
        ] + \
        ['</div>']
    return ''.join(result_list)


@login_required
@require_GET
def trial_next(request, pk):
    trial_exercise: TrialExercise = TrialExercise.objects.get(id=pk)
    user: User = request.user
    if user != trial_exercise.creator:
        return Http404
    if not trial_exercise.is_finished:
        next_word: str
        next_answers: list[str]
        next_word, next_answers = trial_exercise.next_question_and_answers()
        return JsonResponse({'question': next_word, 'answersHTML': generate_html_for_answers(next_answers)})
    else:
        trial_exercise.duration = \
            timezone.now() - trial_exercise.creation_time
        trial_exercise.save()
        return JsonResponse({'stop': True, 'address': '/tasks/trial/{}/result'.format(pk)})


@login_required
@require_GET
def show_trial_results(request, pk):
    trial_exercise: TrialExercise = TrialExercise.objects.get(id=pk)
    user = request.user
    if user == trial_exercise.creator:
        duration = {'minutes': int(trial_exercise.duration.total_seconds() // 60),
                    'seconds': trial_exercise.duration.seconds % 60}
        return render(request, 'trials/result_trial.html',
                      {
                          'progress': trial_exercise.progress,
                          'duration': duration,
                          'group': trial_exercise.dictionary.group,
                          'dictionary_id': trial_exercise.dictionary_id,
                          'star_count': int(5 * trial_exercise.progress[0] / trial_exercise.progress[1])
                      })
    else:
        return Http404
