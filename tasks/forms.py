from django import forms

from dictionaries.models import Dictionary


def create_directions(dictionary: Dictionary) -> list[str]:
    languages = dictionary.languages
    template_direction: str = 'from {} to {}'
    directions = [
        template_direction.format(languages[0].title(), languages[1].title()),
        template_direction.format(languages[1].title(), languages[0].title()),
    ]
    return directions


class CreateExerciseForm(forms.Form):
    direction = forms.ChoiceField(label='Direction', choices=[[0, '1)'], [1, '2)']], initial=0)
    mode = forms.ChoiceField(label='Mode', choices=[['training', 'training'], ['trial', 'trial']], initial='training')
    length = forms.IntegerField(label='Count of words', min_value=1, initial=20)

    def __init__(self, dictionary: Dictionary, *args, **kwargs):
        self.languages = dictionary.languages
        self.directions = create_directions(dictionary)
        super().__init__(*args, **kwargs)
        self.fields['direction'].choices = [[0, self.directions[0]], [1, self.directions[1]]]
