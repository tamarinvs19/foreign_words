from django.contrib import admin
from .models import TrainingExercise, TrialExercise, Task

admin.site.register(TrainingExercise)
admin.site.register(TrialExercise)
admin.site.register(Task)
