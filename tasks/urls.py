from django.urls import path

from . import views

app_name = 'tasks'
urlpatterns = [
    path('choose_dictionary/', views.choose_dictionary, name='exercises'),

    path('create_exercise/<int:dict_id>/', views.create_exercise, name='create_exercise'),

    path('training/<int:pk>/', views.training, name='training'),
    path('training/<int:pk>/next', views.training_next, name='training_next'),
    path('training/<int:pk>/result', views.show_results, name='show_results'),

    path('trial/<int:pk>/', views.trial, name='trial'),
    path('trial/<int:pk>/next', views.trial_next, name='trial_next'),
    path('trial/<int:pk>/result', views.show_trial_results, name='show_trial_results'),
]
