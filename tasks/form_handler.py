from tasks.models import TrainingExercise, TrialExercise
from dictionaries.models import Dictionary, Word
from users.models import Person


class ExerciseGenerator(object):
    @staticmethod
    def check_language(language: str, dictionary: Dictionary) -> bool:
        return language in dictionary.languages and \
                all(
                    language in Word.objects.get(pk=word_id).get_languages
                    for word_id in dictionary.words_id
                )

    @staticmethod
    def generate_training_by_dictionary(
            creator: Person,
            dictionary: Dictionary,
            language: str,
            length: int
    ) -> TrainingExercise:
        if ExerciseGenerator.check_language(language, dictionary):
            training: TrainingExercise = TrainingExercise.objects.create(
                creator=creator, words_id=dictionary.get_random_words(length), language=language,
                dictionary=dictionary
            )
            return training
        else:
            raise ValueError('Incorrect `language` or content of `dictionary`.')

    @staticmethod
    def generate_trial_by_dictionary(
            creator: Person,
            dictionary: Dictionary,
            language: str,
            length: int,
            count_wrong_answers: int
    ) -> TrialExercise:
        if ExerciseGenerator.check_language(language, dictionary):
            words_id: list[int] = dictionary.get_random_words(length)
            wrong_words_id: list[list[int]] = []
            for word_id in words_id:
                wrongs_id: list[int] = []
                while len(wrongs_id) < count_wrong_answers:
                    new_word_id = dictionary.get_random_word()
                    new_word_translation: str = Word.objects.get(pk=new_word_id)\
                        .get_word_and_translation(language)[1]
                    if new_word_id != word_id and \
                        all(Word.objects.get(pk=wrong_id).get_word_and_translation(language)[1]
                            != new_word_translation
                            for wrong_id in wrongs_id):
                        wrongs_id.append(new_word_id)
                wrong_words_id.append(wrongs_id)

            trial: TrialExercise = TrialExercise.objects.create(
                creator=creator, words_id=words_id, language=language,
                wrong_words_id=wrong_words_id, dictionary=dictionary
            )
            return trial
        else:
            raise ValueError('Incorrect `language` or content of `dictionary`.')


class FormHandler(object):
    @staticmethod
    def create_training(creator, form, dictionary) -> TrainingExercise:
        cd: dict = form.cleaned_data
        language = dictionary.languages[int(cd['direction'])]
        length = cd['length']
        return ExerciseGenerator.generate_training_by_dictionary(creator, dictionary, language, length)

    @staticmethod
    def create_trial(creator, form, dictionary) -> TrialExercise:
        count_wrong_answers: int = min(7, dictionary.size - 1)
        cd: dict = form.cleaned_data
        language = dictionary.languages[int(cd['direction'])]
        length = cd['length']
        return ExerciseGenerator.generate_trial_by_dictionary(
            creator, dictionary, language, length, count_wrong_answers)
