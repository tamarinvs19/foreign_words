from unittest.mock import patch

from django.test import TestCase

from tasks.models import TrainingExercise, TrialExercise
from dictionaries.models import WordTemplate, Word
from users.models import Person


class TestTrainingExercise(TestCase):
    def setUp(self) -> None:
        self.english_word_list: list[WordTemplate] = [
            WordTemplate('english', 'badger'),
            WordTemplate('english', 'run'),
            WordTemplate('english', 'linux'),
        ]
        self.russian_word_list: list[WordTemplate] = [
            WordTemplate('russian', 'барсук'),
            WordTemplate('russian', 'бежать'),
            WordTemplate('russian', 'линукс'),
        ]
        self.words: list[Word] = []
        for lang1, lang2 in zip(self.english_word_list, self.russian_word_list):
            word: Word = Word(language1=lang1, language2=lang2)
            word.save()
            self.words.append(word)

        self.words_id: list[int] = [word.id for word in self.words]

        self.user = Person.objects.create(username='Test', id=1)
        self.user.save()

    def test_is_finished_false(self):
        exercise: TrainingExercise = TrainingExercise(
            words_id=self.words_id,
            language='english'
        )
        self.assertFalse(exercise.is_finished)

    def test_is_finished_true(self):
        exercise: TrainingExercise = TrainingExercise(
            words_id=self.words_id,
            language='english',
            current_step=len(self.words_id)
        )
        self.assertTrue(exercise.is_finished)

    def test_progress_init(self):
        exercise: TrainingExercise = TrainingExercise(
            words_id=self.words_id,
            language='english'
        )
        self.assertEqual((0, len(self.words_id)), exercise.progress)

    def test_progress(self):
        exercise: TrainingExercise = TrainingExercise(
            words_id=self.words_id,
            language='english',
            current_step=2,
            right_answers_id=[0]
        )
        self.assertEqual((1, len(self.words_id)), exercise.progress)

    def test_percent_progress_1(self):
        exercise: TrainingExercise = TrainingExercise(
            words_id=self.words_id[:2],
            language='english',
            current_step=1,
            right_answers_id=[]
        )
        self.assertEqual((0, 2), exercise.progress)

    def test_percent_progress_2(self):
        exercise: TrainingExercise = TrainingExercise(
            words_id=self.words_id,
            language='english',
            current_step=3,
            right_answers_id=[2]
        )
        self.assertEqual((1, 3), exercise.progress)

    def test_next_question_finished(self):
        exercise: TrainingExercise = TrainingExercise(
            words_id=self.words_id,
            language='english',
            current_step=(len(self.words_id)),
            right_answers_id=[0]
        )
        with self.assertRaisesMessage(StopIteration, 'All questions were returned') as _:
            exercise.next_question()

    def test_next_question_normal(self):
        exercise: TrainingExercise = TrainingExercise(
            words_id=self.words_id,
            language='english',
            current_step=1,
            right_answers_id=[0]
        )
        self.assertEqual(
            Word.objects.get(pk=self.words_id[1]).id,
            exercise.next_question().id
        )

    def test_save_result(self):
        exercise: TrainingExercise = TrainingExercise(
            words_id=self.words_id,
            language='english',
            creator=self.user,
        )
        exercise.save()

        exercise.save_result(True)
        self.assertEqual(1, exercise.current_step)
        self.assertEqual(1, len(exercise.right_answers_id))

        exercise.save_result(False)
        self.assertEqual(2, exercise.current_step)
        self.assertEqual(1, len(exercise.right_answers_id))

        exercise.save_result(True)
        self.assertEqual(3, exercise.current_step)
        self.assertEqual(2, len(exercise.right_answers_id))


class TestTrialExercise(TestCase):
    def setUp(self) -> None:
        self.english_word_list: list[WordTemplate] = [
            WordTemplate('english', 'badger'),
            WordTemplate('english', 'run'),
            WordTemplate('english', 'linux'),
        ]
        self.russian_word_list: list[WordTemplate] = [
            WordTemplate('russian', 'барсук'),
            WordTemplate('russian', 'бежать'),
            WordTemplate('russian', 'линукс'),
        ]
        self.words: list[Word] = []
        for lang1, lang2 in zip(self.english_word_list, self.russian_word_list):
            word: Word = Word(language1=lang1, language2=lang2)
            word.save()
            self.words.append(word)

        self.words_id: list[int] = [word.id for word in self.words]

        self.user = Person.objects.create(username='Test', id=1)
        self.user.save()

    def test_get_right_answer_1(self):
        exercise: TrialExercise = TrialExercise(
            words_id=self.words_id,
            language='english',
            current_step=0,
            right_answers_id=[]
        )
        word: Word = Word.objects.get(pk=self.words_id[0])
        self.assertEqual(word.id, exercise.get_right_answer().id)

    def test_get_right_answer_2(self):
        exercise: TrialExercise = TrialExercise(
            words_id=self.words_id,
            language='english',
            current_step=1,
            right_answers_id=[0]
        )
        word: Word = Word.objects.get(pk=self.words_id[1])
        self.assertEqual(word.id, exercise.get_right_answer().id)

    def test_get_right_answer_stop(self):
        exercise: TrialExercise = TrialExercise(
            words_id=self.words_id,
            language='english',
            current_step=len(self.words_id),
            right_answers_id=[0]
        )
        with self.assertRaisesMessage(StopIteration, 'All questions were returned') as _:
            exercise.get_right_answer()

    def test_next_question_and_answers(self):
        exercise: TrialExercise = TrialExercise(
            words_id=self.words_id,
            wrong_words_id=[[1, 2, 3], [1, 3, 2], [3, 2, 1]],
            language='english',
            current_step=1,
            right_answers_id=[0],
            creator=self.user
        )
        exercise.save()
        next_question: (str, list[str]) = exercise.next_question_and_answers()
        self.assertEqual(('run', ['барсук', 'бежать', 'бежать', 'линукс']),
                         (next_question[0], sorted(next_question[1])))
