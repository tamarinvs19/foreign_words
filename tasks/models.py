import datetime
import random
from functools import reduce

from django.db import models
from django.utils import timezone
from icecream import ic

from dictionaries.models import Word, Dictionary
from users.models import Person
from groups.models import Group


class Statistic(object):
    def __init__(self, right: int, total: int) -> None:
        self.right = right
        self.total = total

    @property
    def percent(self) -> int:
        return round(100 * self.right / self.total)


class TrainingExercise(models.Model):
    creator: Person = models.ForeignKey('users.Person', on_delete=models.CASCADE)
    words_id: list[int] = models.JSONField(default=list)
    language: str = models.CharField(max_length=100, default='')
    current_step: int = models.IntegerField(default=0)
    right_answers_id: list[int] = models.JSONField(default=list)

    creation_time: datetime.datetime = models.DateTimeField(auto_now_add=True)
    duration: datetime.timedelta = models.DurationField(null=True)

    dictionary: Dictionary = models.ForeignKey('dictionaries.Dictionary', on_delete=models.CASCADE, null=True)

    def __repr__(self) -> str:
        return 'TrainingExercise(creator={}, current_step={}, creation_time={})'. \
            format(self.creator, self.current_step, self.creation_time)

    def __str__(self) -> str:
        return 'Training: created {}'.format(self.creation_time.strftime('%d %h %Y %H:%M'))

    @property
    def is_finished(self) -> bool:
        return self.current_step >= len(self.words_id)

    @property
    def progress(self) -> tuple[int, int]:
        return len(self.right_answers_id), len(self.words_id)

    @property
    def percent_progress(self) -> int:
        return round(self.current_step / self.progress[1] * 100)

    @property
    def second_language(self) -> str:
        first_word: Word = Word.objects.get(id=self.words_id[0])
        languages: list[str] = first_word.get_languages
        languages.remove(self.language)
        second_language: str = languages[0]
        return second_language

    def save_result(self, result: bool) -> None:
        if result:
            self.right_answers_id.append(self.words_id[self.current_step])
        self.current_step += 1
        self.save()

    def next_question(self) -> Word:
        if self.is_finished:
            raise StopIteration('All questions were returned')
        return Word.objects.get(pk=self.words_id[self.current_step])

    def get_statistic_word(self, word_id: int) -> Statistic:
        return Statistic(
            self.right_answers_id.count(word_id),
            self.words_id.count(word_id)
        )

    def get_statistic(self) -> dict[int, Statistic]:
        statistic: dict[int, Statistic] = {}
        for word_id in self.words_id:
            if word_id not in statistic.keys():
                statistic[word_id] = self.get_statistic_word(word_id)
        return statistic


class TrialExercise(TrainingExercise):
    wrong_words_id: list[list[int]] = models.JSONField(default=list)

    def __repr__(self) -> str:
        return 'TrialExercise(creator={}, current_step={}, creation_time={})'. \
            format(self.creator, self.current_step, self.creation_time)

    def __str__(self) -> str:
        return 'Trial: created {}'.format(self.creation_time.strftime('%d %h %Y %H:%M'))

    def get_right_answer(self) -> Word:
        if self.is_finished:
            raise StopIteration('All questions were returned')
        return Word.objects.get(pk=self.words_id[self.current_step])

    def next_question_and_answers(self) -> (str, list[str]):
        if self.is_finished:
            raise StopIteration('All questions were returned')
        word: Word = Word.objects.get(pk=self.words_id[self.current_step])
        question: str = word.get_word_and_translation(self.language)[0]
        right_answer: str = word.get_word_and_translation(self.language)[1]
        answers: list[str] = [Word.objects.get(pk=word_id).get_word_and_translation(self.language)[1]
                              for word_id in self.wrong_words_id[self.current_step]] + [right_answer]
        random.shuffle(answers)
        return question, answers


def get_statistic_list(trainings: list[TrainingExercise]) -> dict[int, Statistic]:
    statistic: dict[int, Statistic] = {}
    for training in trainings:
        statistic = merge_statistic_dicts(statistic, training.get_statistic())
    return statistic


def merge_pair_statistics(statistic1: Statistic, statistic2: Statistic):
    return Statistic(
        statistic1.right + statistic2.right,
        statistic1.total + statistic2.total
    )


def merge_statistic_dicts(dict1: dict[int, Statistic], dict2: dict[int, Statistic]):
    merge_statistic: dict[int, Statistic] = {}
    word_id: int
    for word_id in {*dict1.keys(), *dict2.keys()}:
        if word_id in dict1.keys() and word_id in dict2.keys():
            merge_statistic[word_id] = merge_pair_statistics(dict1[word_id], dict2[word_id])
        elif word_id in dict1.keys():
            merge_statistic[word_id] = dict1[word_id]
        elif word_id in dict2.keys():
            merge_statistic[word_id] = dict2[word_id]
    return merge_statistic


class Task(models.Model):
    title: str = models.CharField(max_length=200)
    creation_time: datetime.datetime = models.DateTimeField(auto_now_add=True)
    deadline: datetime.datetime = models.DateTimeField()

    dictionary: Dictionary = models.ForeignKey('dictionaries.Dictionary', on_delete=models.CASCADE)
    creator: Person = models.ForeignKey('users.Person', on_delete=models.CASCADE)
    group: Group = models.ForeignKey('groups.Group', on_delete=models.CASCADE)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.period = 15
        self.minimal_progress = 90

    def __repr__(self) -> str:
        return 'Task(title={}, creation_time={}, deadline={}, dictionary={}, group={}, creator={})'.format(
            self.title, self.creation_time, self.deadline, self.dictionary, self.group, self.creator)

    def __str__(self) -> str:
        return 'Task: {}, deadline: {}'.format(self.title, self.deadline)

    def calculate_progress(self, user: Person) -> int:
        trainings: list[TrainingExercise] = [exercise for exercise in
                                             TrainingExercise.objects.filter(creator=user,
                                                                             dictionary=self.dictionary,
                                                                             creation_time__gte=
                                                                             timezone.now() - 
                                                                             datetime.timedelta(days=self.period)
                                                                             )]
        list_statistics: list[Statistic] = list(get_statistic_list(trainings).values())
        progress: int = sum(1 for statistic in list_statistics if statistic.percent > self.minimal_progress)
        return round(progress / self.dictionary.size * 100)


class RatingData(object):
    def __init__(self, group: Group, time: int = 10) -> None:
        self.group: Group = group
        self.persons: list[Person] = Person.objects.filter(group=group)
        self.time: int = time

    def get_day_progress(self, person: Person, start_date: datetime.datetime) -> list[(int, int)]:
        """
        :return: list[(count of right answers, count of words)]
        """
        exercises: list[TrainingExercise] = list(filter(lambda e: e.creation_time.day == start_date.day,
                                                        TrainingExercise.objects.filter(
                                                            creator=person,
                                                            dictionary__group=self.group,
                                                        )))
        progress: list[(int, int)] = [exercise.progress for exercise in exercises]
        return progress

    def get_progress(self, person: Person) -> list[(int, int, int)]:
        """
        :return list[(count of right answers, count of words, count of trainings)]
        """
        return [
            reduce(lambda acc, x: (x[0], x[1], 1 + acc[2]),
                   self.get_day_progress(person, day), (0, 0, 0))
            for day in [timezone.now().today() - datetime.timedelta(days=x) for x in range(self.time)]
        ]
